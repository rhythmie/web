import { redirect } from '@sveltejs/kit';
import type { Actions } from './$types';
import { PUBLIC_CLOUDFLARE_SECRET } from '$env/static/public';

const SECRET_KEY = PUBLIC_CLOUDFLARE_SECRET;

interface TokenValidateResponse {
	'error-codes': string[];
	success: boolean;
	action: string;
	cdata: string;
}

async function validateToken(token: string, secret: string) {
	const response = await fetch('https://challenges.cloudflare.com/turnstile/v0/siteverify', {
		method: 'POST',
		headers: {
			'content-type': 'application/json'
		},
		body: JSON.stringify({
			response: token,
			secret: secret
		})
	});

	const data: TokenValidateResponse = await response.json();

	return {
		success: data.success,
		error: data['error-codes']?.length ? data['error-codes'][0] : null
	};
}

export const actions: Actions = {
	default: async ({ request, locals: { supabase } }) => {
		const client_data = await request.formData();
		const email = client_data.get('email') as string;
		const password = client_data.get('password') as string;
		const token = client_data.get('cf-turnstile-response');

		console.log('Token received:', token);

		if (!token) {
			return {
				error: 'Token not found in form data'
			};
		}

		const { success, error } = await validateToken(token.toString(), SECRET_KEY);
		console.log('Token validation:', { success, error });

		if (!success) {
			return {
				error: error || 'Invalid CAPTCHA'
			};
		} else {
			const { error } = await supabase.auth.signInWithPassword({
				email: email,
				password: password,
				options: {
					captchaToken: token
				}
			});

			if (error) {
				console.error(error);
			}
		}

		if (error) {
			console.error(error);
		} else {
			return redirect(303, '/');
		}
	}
};
