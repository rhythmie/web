import { type Actions, redirect } from '@sveltejs/kit';
import { supabaseStore } from '../../../store';
import { PUBLIC_CLOUDFLARE_SECRET } from '$env/static/public';

const SECRET_KEY = PUBLIC_CLOUDFLARE_SECRET;

interface TokenValidateResponse {
	'error-codes': string[];
	success: boolean;
	action: string;
	cdata: string;
}

async function validateToken(token: string, secret: string) {
	const response = await fetch('https://challenges.cloudflare.com/turnstile/v0/siteverify', {
		method: 'POST',
		headers: {
			'content-type': 'application/json'
		},
		body: JSON.stringify({
			response: token,
			secret: secret
		})
	});

	const data: TokenValidateResponse = await response.json();

	return {
		success: data.success,
		error: data['error-codes']?.length ? data['error-codes'][0] : null
	};
}

let supabase;

supabaseStore.subscribe((value) => {
	supabase = value;
});

export const actions: Actions = {
	default: async ({ request, locals }) => {
		try {
			const data = await request.formData();
			const token = data.get('cf-turnstile-response'); // if you edited the formsField option change this

			console.log('Token received:', token); // Debugging: Check if token is being received

			if (!token) {
				return {
					error: 'Token not found in form data'
				};
			}

			const { success, error } = await validateToken(token.toString(), SECRET_KEY);

			console.log('Token validation:', { success, error }); // Debugging: Check token validation response

			if (!success) {
				return {
					error: error || 'Invalid CAPTCHA'
				};
			} else {
				const { data: signInData, error: signInError } = await supabase.auth.signInAnonymously({
					options: {
						captchaToken: token
					}
				});

				if (signInError) {
					console.error('Supabase sign-in error:', signInError); // Debugging: Check Supabase error
					return {
						error: signInError.message
					};
				} else {
					console.log('Sign-in data:', signInData); // Debugging: Check Supabase sign-in response

					// Set the session token in cookies
					await locals.supabase.auth.setSession(signInData.session);
				}
			}
		} catch (err) {
			console.error('Unexpected error:', err); // Debugging: Catch any unexpected errors
			return {
				error: 'An unexpected error occurred'
			};
		}

		throw redirect(303, '/'); // Ensure you use 'throw' to trigger redirection
	}
};
