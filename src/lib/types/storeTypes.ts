import type { imageType } from './resultTypes';

interface videoThumbnail {
	quality: string;
	url: string;
	width: number;
	height: number;
}

export interface Queue {
	isVideo: boolean;
	author: string;
	id: string;
	name: string;
	type: string;
	year: string;
	releaseDate: string;
	duration: number;
	label: string;
	explicitContent: boolean;
	playCount: number;
	language: string;
	hasLyrics: boolean;
	lyricsId: string | null;
	url: string;
	copyright: string;
	album: {
		id: string;
		name: string;
		url: string;
	};
	artists: {
		primary: [
			{
				id: string;
				name: string;
				role: string;
				image: imageType[];
				type: string;
				url: string;
			}
		];
		all: [
			{
				id: string;
				name: string;
				role: string;
				image: imageType[];
				type: string;
				url: string;
			}
		];
	};
	image: imageType[];
	downloadUrl: [
		{
			quality: string;
			url: string;
		}
	];
}

export interface VideoResults {
	type: string;
	title: string;
	videoId: string;
	author: string;
	authorId: string;
	authorUrl: string;
	authorVerified: boolean;
	videoThumbnails: [
		videoThumbnail,
		videoThumbnail,
		videoThumbnail,
		videoThumbnail,
		videoThumbnail,
		videoThumbnail,
		videoThumbnail,
		videoThumbnail,
		videoThumbnail
	];
	description: string;
	descriptionHtml: string;
	viewCount: number;
	viewCountText: string;
	published: number;
	publishedText: string;
	lengthSeconds: number;
	liveNow: boolean;
	premium: boolean;
	isUpcoming: boolean;
}

export interface CurrentUser {
	full_name: string;
	email: string;
}

export interface SyncedLyrics {
	result: {
		lyrics: {
			syncType: string;
			lines: [
				{
					startTimeMs: string;
					words: string;
					syllables: [];
					endTimeMs: string;
				}
			];
		};
	};
}

export interface Lyrics {
	result: string[];
}

export interface LyricsState {
    type: 'synced' | 'unsynced';
}