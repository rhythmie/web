import { isLoading, searchQuery, songResults } from '../../../../../store';
import type { PageLoad } from '././$types';

export const load: PageLoad = async ({ url, fetch }) => {
	isLoading.set({
		search: false,
		songs: true,
		albums: false,
		artists: false
	});
	const query = url.searchParams.get('query');
	searchQuery.set(query);
	if (query) {
		try {
			const response = await fetch(
				`https://rhythmie-api.vercel.app/api/search/songs?query=${query}&page=0&limit=20`,
				{
					headers: {
						origin: window.location.origin // Get origin from browser window
					}
				}
			);

			if (!response.ok) {
				throw new Error(`API request failed with status ${response.status}`);
			}

			const jsonData = await response.json();
			isLoading.set({
				search: false,
				songs: false,
				albums: false,
				artists: false
			});
			songResults.set(jsonData);
		} catch (error) {
			console.error('Error fetching data:', error);
		}
	}
};
