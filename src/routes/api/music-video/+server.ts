import { json } from '@sveltejs/kit';
import { invidiousInstance } from '$lib/global';

export const GET = async ({ url }) => {
	const query = url.searchParams.get('query');

	if (!query) {
		return json(
			{ error: 'No query provided' },
			{
				status: 400
			}
		);
	}

	try {
		const results = await fetch(
			`https://${invidiousInstance}/api/v1/search?q=${query}+official+video`
		);
		const parsedResults = await results.json();

		let loop = true;
		let index = 0;

		while (loop) {
			if (index > 2) loop = false;
			// if ()
			const response = await fetch(
				`https://${invidiousInstance}/api/v1/videos/${parsedResults[index].videoId}`
			);
			const parsedData = await response.json();

			const video = await fetch(
				`${parsedData.formatStreams.length > 2 ? parsedData.formatStreams[1].url : parsedData.formatStreams[0].url}`,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					},
					mode: 'cors'
				}
			);

			index++;

			if (video.ok) {
				return json({ parsedData });
			} else {
				loop = true;
			}
		}
		return json({ error: 'Video not found' }, { status: 500 });
	} catch (error) {
		console.error('Error searching YouTube:', error);
		return json({ error: 'Error searching YouTube' }, { status: 500 });
	}
};
