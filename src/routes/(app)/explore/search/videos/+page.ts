import { isLoading, searchQuery, videoResults } from '../../../../../store';
import type { PageLoad } from '././$types';

export const load: PageLoad = ({ url }) => {
	const query = url.searchParams.get('query');
	searchQuery.set(query);
	videoResults.set([]);

	isLoading.set({
		search: false,
		songs: false,
		videos: true,
		albums: false,
		artists: false
	});

	return {
		query
	};
};
