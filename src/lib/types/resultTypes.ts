export interface imageType {
	quality: string;
	url: string;
}

export interface resultType {
	success: {
		type: boolean;
	};
	data: {
		topQuery: {
			results: [
				{
					id: string;
					title: string;
					image: [
						{
							quality: string;
							url: string;
						}
					];
					type: string;
					description: string;
				}
			];
			position: number;
		};
		songs: {
			results: [
				{
					id: string;
					title: string;
					image: imageType[];
					type: string;
					description: string;
				}
			];
			album: string;
			url: string;
			type: string;
			description: string;
			primaryArtists: string;
			singers: string;
			language: string;
			position: number;
		};
		albums: {
			results: [
				{
					id: string;
					title: string;
					image: [
						{
							quality: string;
							url: string;
						}
					];
					type: string;
					description: string;
				}
			];
			artist: string;
			url: string;
			type: string;
			description: string;
			year: string;
			songIds: string;
			language: string;
			position: number;
		};
		artists: {
			results: [
				{
					id: string;
					title: string;
					image: [
						{
							quality: string;
							url: string;
						}
					];
					type: string;
					description: string;
				}
			];
			type: string;
			description: string;
			position: number;
		};
	};
}

export interface songType {
	success: boolean;
	data: [
		{
			id: string;
			name: string;
			type: string;
			year: string;
			releaseDate: string;
			duration: number;
			label: string;
			explicitContent: boolean;
			playCount: number;
			language: string;
			hasLyrics: boolean;
			lyricsId: string | null;
			url: string;
			copyright: string;
			album: {
				id: string;
				name: string;
				url: string;
			};
			artists: {
				primary: [
					{
						id: string;
						name: string;
						role: string;
						image: imageType[];
						type: string;
						url: string;
					}
				];
				all: [
					{
						id: string;
						name: string;
						role: string;
						image: imageType[];
						type: string;
						url: string;
					}
				];
			};
			image: imageType[];
			downloadUrl: [{
				quality: string;
				url: string;
			}];
		}
	];
}

export interface artistsType {
    id: string;
    name: string;
    role: string;
    image: imageType[];
    type: string;
    url: string;
}