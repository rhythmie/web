import type { PageLoad } from '../../../../../.svelte-kit/types/src/routes/(authenticated)/(home)/$types';
import { albumInfo, albumInfoLoading } from '../../../../store';

export const load: PageLoad = async ({ url, fetch }) => {
	albumInfoLoading.set(true);
	const id = url.searchParams.get('id');
	if (id) {
		try {
			const response = await fetch(`https://rhythmie-api.vercel.app/api/albums?id=${id}`, {
				headers: {
					origin: window.location.origin // Get origin from browser window
				}
			});
			if (!response.ok) {
				throw new Error(`API request failed with status ${response.status}`);
			}

			const jsonData = await response.json();
			console.log(jsonData);
			albumInfo.set(jsonData);
			albumInfoLoading.set(false);
		} catch (error) {
			console.error(error);
		}
	}
};
