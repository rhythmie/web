import { json } from '@sveltejs/kit';

export const GET = async () => {
	try {
		console.log('Fetching home data...');
		const { default: YTMusic } = await import('ytmusic-api');
		const ytmusic = new YTMusic.default(); // Adjusting the constructor usage
		await ytmusic.initialize();
		const result = await ytmusic.getHome();
		return json({ result });
	} catch (error) {
		console.error('Error fetching homepage data:', error);
		return json({ error: 'Error fetching homepage data', status: 500 });
	}
};
