import type { Fetch } from '@supabase/supabase-js/dist/module/lib/types';
import { json } from '@sveltejs/kit';

export const GET = async ({ url, fetch }: { url: URL, fetch: Fetch }) => {
	const query = url.searchParams.get('query');

	if (!query) {
		return json(
			{ error: 'No query provided' },
			{
				status: 400
			}
		);
	}

	try {
		const { default: YTMusic } = await import('ytmusic-api');
		// @ts-expect-error - This is a dynamic import
		const ytmusic = new YTMusic.default(); // Adjusting the constructor usage
		await ytmusic.initialize();

		const results = await ytmusic.searchSongs(query);
		const syncedLyrics = await fetch(`https://lyrix.vercel.app/getLyricsByName/${results[0].artist.name}/${results[0].name}`);

		if (syncedLyrics.ok) {
			const result = await syncedLyrics.json();
			return json({ result });
		}
		else {
			const result = await ytmusic.getLyrics(results[0].videoId);
			return json({ result });
		}

	} catch (error) {
		console.error('Error searching YouTube Music:', error);
		return json({ error: 'Error searching YouTube Music' }, { status: 500 });
	}
};
