import { artistInfo, artistInfoLoading } from '../../../../store';
import type { PageLoad } from '../../../../../.svelte-kit/types/src/routes/(app)/$types';

export const load: PageLoad = async ({ url, fetch }) => {
	artistInfoLoading.set(true);
	const id = url.searchParams.get('id');
	if (id) {
		try {
			const response = await fetch(`https://rhythmie-api.vercel.app/api/artists/${id}`, {
				headers: {
					origin: window.location.origin // Get origin from browser window
				}
			});
			if (!response.ok) {
				throw new Error(`API request failed with status ${response.status}`);
			}

			const jsonData = await response.json();
			console.log(jsonData);
			artistInfo.set(jsonData);
			artistInfoLoading.set(false);
		} catch (error) {
			console.error(error);
		}
	}
};
