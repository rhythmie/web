import type { RequestHandler } from '@sveltejs/kit';
import { invidiousInstance } from '$lib/global';
import { json } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ url, fetch }) => {
	const videoId = url.searchParams.get('videoId');

	if (!videoId) {
		console.error('No videoId provided');
	} else {
		try {
			const response = await fetch(`https://${invidiousInstance}/api/v1/videos/${videoId}`);
			const data = await response.json();

			return json({ data });
		} catch (err) {
			console.error(err);
			return json({ error: 'Error fetching the video', status: 500 });
		}
	}
};
