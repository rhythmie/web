import { json } from '@sveltejs/kit';
import type { RequestHandler } from '@sveltejs/kit';
import { invidiousInstance } from '$lib/global';

export const GET: RequestHandler = async ({ url }) => {
	const query = url.searchParams.get('query');
	const limit = Number(url.searchParams.get('limit'));

	if (!query) {
		return json(
			{ error: 'No query provided' },
			{
				status: 400
			}
		);
	}

	try {
		const results = await fetch(`https://${invidiousInstance}/api/v1/search?q=${query}`);
		let parsedResults = await results.json();

		if (limit) {
			parsedResults = parsedResults.slice(0, limit);
		}

		const stream = new ReadableStream({
			async start(controller) {
				const encoder = new TextEncoder();

				for (const result of parsedResults) {
					try {
						const response = await fetch(
							`https://${invidiousInstance}/api/v1/videos/${result.videoId}`
						);

						if (!response.ok) {
							throw new Error('Failed to fetch video data');
						}

						const parsedData = await response.json();

						const videoUrl =
							parsedData.formatStreams.length > 2
								? parsedData.formatStreams[1].url
								: parsedData.formatStreams[0].url;

						const video = await fetch(videoUrl, {
							method: 'GET',
							headers: {
								'Content-Type': 'application/json'
							},
							mode: 'cors'
						});

						if (video.ok) {
							controller.enqueue(encoder.encode(JSON.stringify(result) + '\n'));
						}
					} catch (error: Error) {
						console.error('Error fetching video:', error);
						controller.enqueue(encoder.encode(JSON.stringify({ error: error.message }) + '\n'));
					}
				}

				controller.close();
			}
		});

		return new Response(stream, {
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			}
		});
	} catch (error) {
		console.error('Error searching for the video:', error);
		return json({ error: 'Error searching for the video' }, { status: 500 });
	}
};
