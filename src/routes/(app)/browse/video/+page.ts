import { videoInfo, videoInfoLoading } from '../../../../store';
import type { PageLoad } from '../../../../../.svelte-kit/types/src/routes/(app)/$types';

export const load: PageLoad = async ({ url, fetch }) => {
	videoInfoLoading.set(true);
	const id = url.searchParams.get('id');
	if (id) {
		try {
			const response = await fetch(`/api/fetch-video?videoId=${id}`, {
				headers: {
					origin: window.location.origin // Get origin from browser window
				}
			});

			if (!response.ok) {
				throw new Error(`API request failed with status ${response.status}`);
			}

			const data = await response.json();

			console.log(data);
			videoInfo.set(data);
			videoInfoLoading.set(false);
		} catch (error) {
			console.error(error);
		}
	}
};
