export interface homeDataType {
    title: string,
    contents: [{
        name: string,
        playlistId: string,
        thumbnails: [{
            height: number,
            url: string,
            width: number
        }],
        type: string,
    }]
}
