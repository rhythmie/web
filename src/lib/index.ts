import {
	isLoading,
	musicVideoLoaded,
	musicVideoState,
	musicVideoURL,
	nowPlayingToggle,
	playerData,
	playState,
	queue,
	suggestionsState,
	videoResults
} from '../store';
import type { artistsType, songType } from './types/resultTypes';
import type { Queue } from './types/storeTypes';

// export const appendToLocalStorage = (key, value) => {
// 	// Retrieve the current array from localStorage
// 	let currentArray = localStorage.getItem(key);

// 	// Parse the retrieved value or initialize as an empty array
// 	currentArray = currentArray ? currentArray.split(',') : [];

// 	// Check if the new value already exists in the array
// 	const valueIndex = currentArray.indexOf(value);
// 	if (valueIndex > -1) {
// 		// Remove the existing value from its current position
// 		currentArray.splice(valueIndex, 1);
// 	}

// 	// Add the new value to the top of the array
// 	currentArray.unshift(value);

// 	// Convert the updated array back to a comma-separated string
// 	const updatedArrayString = currentArray.join(',');

// 	// Save the updated string back to localStorage
// 	localStorage.setItem(key, updatedArrayString);
// };

export const appendToLocalStorage = (key: string, value: string) => {
	// Retrieve the current array from localStorage
	const currentArray: string | null = localStorage.getItem(key);

	// Parse the retrieved value or initialize as an empty array
	const array: string[] = currentArray ? currentArray.split(',') : [];

	// Check if the new value already exists in the array
	const valueIndex = array.indexOf(value);
	if (valueIndex > -1) {
		// Remove the existing value from its current position
		array.splice(valueIndex, 1);
	}

	// Add the new value to the top of the array
	array.unshift(value);

	// Convert the updated array back to a comma-separated string
	const updatedArrayString = array.join(',');

	// Save the updated string back to localStorage
	localStorage.setItem(key, updatedArrayString);
};

export const appendObjectToLocalStorage = (key: string, value: object) => {
	// Retrieve the current array from localStorage
	const currentArray: Array<object> = JSON.parse(
		localStorage.getItem(key)?.toString() || '[]'
	) as Array<object>;

	// Ensure currentArray is indeed an array
	if (Array.isArray(currentArray)) {
		// Check if the new object already exists in the array
		const objectIndex = currentArray.findIndex(
			(item) => JSON.stringify(item) === JSON.stringify(value)
		);
		if (objectIndex > -1) {
			// Remove the existing object from its current position
			currentArray.splice(objectIndex, 1);
		}

		// Add the new object to the top of the array
		currentArray.unshift(value);

		// Convert the updated array back to a JSON string
		const updatedArrayString = JSON.stringify(currentArray);

		// Save the updated string back to localStorage
		localStorage.setItem(key, updatedArrayString);
	}
};

export const limitString = (str: string, limit: number) => {
	if (str !== undefined) {
		if (str.length > limit) {
			return `${str.slice(0, limit)}...`;
		} else {
			return str;
		}
	} else {
		return null;
	}
};

export const artists = (arr: artistsType[], filter: null | string = null) => {
	let text = '';
	arr.forEach((value, index) => {
		if (filter !== null) {
			if (value.role === filter) {
				if (index < arr.length - 1) {
					text += `${value.name}, `;
				} else {
					text += value.name;
				}
			}
		} else {
			if (index < arr.length - 1) {
				text += `${value.name}, `;
			} else {
				text += value.name;
			}
		}
	});
	return text;
};

export const formatNumber = (num: number) => {
	if (num) {
		if (num >= 1e9) {
			return `${(num / 1e9).toFixed(1)}B`; // Billions
		} else if (num >= 1e6) {
			return `${(num / 1e6).toFixed(1)}M`; // Millions
		} else if (num >= 1e3) {
			return `${(num / 1e3).toFixed(1)}k`; // Thousands
		} else {
			return num.toString(); // Less than a thousand
		}
	} else {
		return null;
	}
};

export const timeConverter = (timestamp: number) => {
	const now = Number(new Date());
	const timeDifference = now - Number(new Date(timestamp * 1000)); // convert seconds to milliseconds

	const seconds = Math.floor(timeDifference / 1000);
	const minutes = Math.floor(seconds / 60);
	const hours = Math.floor(minutes / 60);
	const days = Math.floor(hours / 24);
	const weeks = Math.floor(days / 7);
	const months = Math.floor(days / 30);
	const years = Math.floor(days / 365);

	if (years > 0) {
		return `${years} years ago`;
	} else if (months > 0) {
		return `${months} months ago`;
	} else if (weeks > 0) {
		return `${weeks} weeks ago`;
	} else if (days > 0) {
		return `${days} days ago`;
	} else if (hours > 0) {
		return `${hours} hours ago`;
	} else if (minutes > 0) {
		return `${minutes} minutes ago`;
	} else {
		return `${seconds} seconds ago`;
	}
};

export const converter = (value: number | string) => {
	const val: number = Math.round(Number(value));
	const hour = Math.floor(val / 3600);
	const minute = Math.floor((val % 3600) / 60);
	const second = Math.floor((val % 3600) % 60);
	const hours = hour > 0 ? `${hour}:` : '';
	const minutes = minute > 0 ? `${minute < 10 ? `0${minute}` : minute}:` : '00:';
	const seconds = second > 0 ? (second < 10 ? `0${second}` : second) : '00';
	return hours + minutes + seconds;
};
export const enqueue = (item: Queue) => {
	queue.update((prevItems) => [...prevItems, item]);
};

export const dequeue = () => {
	queue.update((prevItems) => prevItems.slice(1));
};

export const fetchVideo = async (songName: string, songArtists: artistsType[]) => {
	musicVideoURL.set(undefined);
	musicVideoLoaded.set(false);
	const response = await fetch(`/api/music-video?query=${songName}+${artists(songArtists)}`);
	const parsedData = await response.json();
	if (response.ok) {
		musicVideoURL.set(
			`${parsedData.parsedData.formatStreams.length > 1 ? parsedData.parsedData.formatStreams[1].url : parsedData.parsedData.formatStreams[0].url}`
		);
		musicVideoLoaded.set(true);
	} else {
		musicVideoURL.set(undefined);
		musicVideoLoaded.set(false);
	}
};

export const fetchVideoById = async (videoId: string) => {
	musicVideoURL.set(undefined);
	musicVideoLoaded.set(false);
	const response = await fetch(`/api/fetch-video?videoId=${videoId}`);
	const results = await response.json();
	if (response.ok) {
		nowPlayingToggle.set(false);
		musicVideoState.set(true);
		musicVideoURL.set(
			`${results.data.formatStreams.length > 1 ? results.data.formatStreams[1].url : results.data.formatStreams[0].url}`
		);
		musicVideoLoaded.set(true);
		playerData.set({
			isVideo: true,
			id: results.data.videoId,
			name: results.data.title,
			author: results.data.author,
			image: results.data.videoThumbnails,
			duration: results.data.lengthSeconds,
			type: '',
			year: '',
			releaseDate: '',
			label: '',
			explicitContent: false,
			playCount: 0,
			language: '',
			hasLyrics: false,
			lyricsId: null,
			url: '',
			copyright: '',
			album: {
				id: '',
				name: '',
				url: ''
			},
			artists: {
				primary: [
					{
						id: '',
						name: '',
						role: '',
						image: [
							{
								quality: '',
								url: ''
							}
						],
						type: '',
						url: ''
					}
				],
				all: [
					{
						id: '',
						name: '',
						role: '',
						image: [
							{
								quality: '',
								url: ''
							}
						],
						type: '',
						url: ''
					}
				]
			},
			downloadUrl: [
				{
					quality: '',
					url: ''
				}
			]
		});
		queue.set([
			{
				isVideo: true,
				id: results.data.videoId,
				name: results.data.title,
				author: results.data.author,
				image: results.data.videoThumbnails,
				duration: results.data.lengthSeconds,
				type: '',
				year: '',
				releaseDate: '',
				label: '',
				explicitContent: false,
				playCount: 0,
				language: '',
				hasLyrics: false,
				lyricsId: null,
				url: '',
				copyright: '',
				album: {
					id: '',
					name: '',
					url: ''
				},
				artists: {
					primary: [
						{
							id: '',
							name: '',
							role: '',
							image: [
								{
									quality: '',
									url: ''
								}
							],
							type: '',
							url: ''
						}
					],
					all: [
						{
							id: '',
							name: '',
							role: '',
							image: [
								{
									quality: '',
									url: ''
								}
							],
							type: '',
							url: ''
						}
					]
				},
				downloadUrl: [
					{
						quality: '',
						url: ''
					}
				]
			}
		]);
	} else {
		musicVideoURL.set(undefined);
		musicVideoLoaded.set(false);
	}
};

export const searchVideos = async (query: string, limit: number | undefined = undefined) => {
	videoResults.set(undefined);
	try {
		let result = '';
		let tries = 0;
		let response;

		if (limit) response = await fetch(`/api/search-videos?query=${query}&limit=${limit}`);
		else response = await fetch(`/api/search-videos?query=${query}`);

		if (response.ok && response.body) {
			const reader = response.body.getReader();
			const decoder = new TextDecoder('utf-8');

			while (tries < 20) {
				const { done, value } = await reader.read();
				if (done) break;
				result += decoder.decode(value);
				const lines = result.split('\n');

				for (let i = 0; i < lines.length - 1; i++) {
					if (lines[i]) {
						try {
							const video = JSON.parse(lines[i]);
							videoResults.update((prevItems) => {
								// If prevItems is not an array, convert it to one using [].concat()
								if (!Array.isArray(prevItems)) {
									prevItems = [].concat(prevItems || []);
								} else if (!prevItems) {
									prevItems = [];
								}
								return [...prevItems, video];
							});
						} catch (error) {
							console.error('Error parsing video data:', error);
						}
					}
				}

				// Keep the remaining incomplete line (if any) for the next read
				result = lines[lines.length - 1];
				tries++;
			}

			isLoading.set({
				search: false,
				songs: false,
				videos: false,
				albums: false,
				artists: false
			});
		}
	} catch (error) {
		console.error('Error fetching data:', error);
	}
};

let suggestions = false;
let songId: string;

suggestionsState.subscribe(async (value) => {
	suggestions = value;

	if (value && songId) {
		try {
			const rawSuggestions = await fetch(
				`https://rhythmie-api.vercel.app/api/songs/${songId}/suggestions`
			);
			if (!rawSuggestions.ok) {
				// Log the HTTP status and statusText
				console.error(`HTTP Error: ${rawSuggestions.status} ${rawSuggestions.statusText}`);
				return;
			}

			const parsedSuggestions = await rawSuggestions.json();
			if (parsedSuggestions && parsedSuggestions.data) {
				parsedSuggestions.data.forEach((song: Queue) => {
					enqueue(song);
				});
			} else {
				console.error('Invalid data structure:', parsedSuggestions);
			}
		} catch (err) {
			console.error('Failed to fetch suggestions:', err);
		}
	}
});

export const fetchSong = async (songID: string, clearQueue: boolean) => {
	songId = songID;
	try {
		musicVideoURL.set(undefined);
		musicVideoLoaded.set(false);
		const data = await fetch(`https://rhythmie-api.vercel.app/api/songs/${songID}`);
		const parsedData: songType = await data.json();
		const playerRawData = {
			...parsedData.data[0],
			isVideo: false,
			author: ''
		};
		playState.set(true);
		nowPlayingToggle.set(false);

		if (clearQueue) {
			playerData.set(playerRawData);

			if (suggestions) {
				try {
					const rawSuggestions = await fetch(
						`https://rhythmie-api.vercel.app/api/songs/${songID}/suggestions`
					);
					if (!rawSuggestions.ok) {
						// Log the HTTP status and statusText
						console.error(`HTTP Error: ${rawSuggestions.status} ${rawSuggestions.statusText}`);
						return;
					}

					const parsedSuggestions = await rawSuggestions.json();
					if (parsedSuggestions && parsedSuggestions.data) {
						parsedSuggestions.data.forEach((song: Queue) => {
							enqueue(song);
						});
					} else {
						console.error('Invalid data structure:', parsedSuggestions);
					}
				} catch (err) {
					console.error('Failed to fetch suggestions:', err);
				}
			} else {
				queue.set(
					parsedData.data.map((item) => ({
						...item,
						isVideo: false,
						author: ''
					}))
				);
			}
		} else {
			playerData.set(playerRawData);
		}
		await fetchVideo(parsedData.data[0].name, parsedData.data[0].artists.primary);
	} catch (err) {
		console.error(err);
	}
};

export const playSongInSequence = async (array: Queue[], index: number) => {
	queue.set([]);
	const slicedResults = array.slice(index);
	slicedResults.forEach((song: Queue) => {
		enqueue(song);
	});
	playerData.set(array[index]);
	await fetchVideo(array[index].name, array[index].artists.primary);
};

export const debounce = <F extends (...args: unknown[]) => unknown>(
	func: F,
	wait: number,
	immediate = false
): ((...args: Parameters<F>) => void) => {
	let timeout: ReturnType<typeof setTimeout> | null = null;

	return (...args: Parameters<F>) => {
		const later = () => {
			timeout = null;
			func.apply(this, args);
		};

		const callNow = immediate && !timeout;
		if (timeout) {
			clearTimeout(timeout);
		}
		timeout = setTimeout(later, wait);

		if (callNow) func.apply(this, args);
	};
};
