import { songInfo, songInfoLoading } from '../../../../store';
import type { PageLoad } from '../../../../../.svelte-kit/types/src/routes/(app)/$types';

export const load: PageLoad = async ({ url, fetch }) => {
	songInfoLoading.set(true);
	const id = url.searchParams.get('id');
	if (id) {
		try {
			const response = await fetch(`https://rhythmie-api.vercel.app/api/songs/${id}`, {
				headers: {
					origin: window.location.origin // Get origin from browser window
				}
			});
			if (!response.ok) {
				throw new Error(`API request failed with status ${response.status}`);
			}

			const jsonData = await response.json();
			console.log(jsonData);
			songInfo.set(jsonData);
			songInfoLoading.set(false);
		} catch (error) {
			console.error(error);
		}
	}
};
