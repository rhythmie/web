import type { imageType } from "./resultTypes";

export interface isLoadingType {
    search: boolean;
    songs: boolean;
    videos: boolean;
    albums: boolean;
    artists: boolean;
}

export interface LastPlayedSong {
    id: string,
    name: string,
    artists: string,
    image: imageType[]
}

export interface LikedSongs {
    id: string,
    name: string,
    artists: string,
    image: imageType[]
}