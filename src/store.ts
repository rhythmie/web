// Imports
import type { resultType } from '$lib/types/resultTypes';
import type { CurrentUser, Lyrics, LyricsState, Queue, SyncedLyrics, VideoResults } from '$lib/types/storeTypes';
import type { isLoadingType } from '$lib/types/types';
import type { SupabaseClient } from '@supabase/supabase-js';
import { type Writable, writable } from 'svelte/store';

// Auth session data
export const activeSession: Writable<boolean> = writable(false);
export const currentUser: Writable<CurrentUser> = writable();
export const supabaseStore: Writable<SupabaseClient> = writable();

// Dialog states
export const documentationState: Writable<boolean> = writable(false);
export const qualitySettingState: Writable<boolean> = writable(false);
export const alertState: Writable<boolean> = writable(false);

// Alert data
export const alertIcon: Writable<string> = writable('info');
export const alertMessage: Writable<string> = writable('This is an alert');

// Homepage data
export const greeting: Writable<string> = writable('Hey there!');
export const greetingStyle: Writable<string> = writable(
	'https://c.tenor.com/Wx9IEmZZXSoAAAAi/hi.gif'
);

// Search component data
export const searchQuery: Writable<string> = writable();
export const showSuggestions: Writable<boolean> = writable(false);

// Loading state
export const isLoading: Writable<isLoadingType> = writable({
	search: true,
	songs: true,
	videos: true,
	albums: true,
	artists: true
});

// Player state
export const miniPlayerState: Writable<boolean> = writable(true);
export const nowPlayingToggle: Writable<boolean> = writable(false);
export const playState: Writable<boolean> = writable(false);
export const playerContextMenuState: Writable<boolean> = writable(false);
export const musicVideoData: Writable<object> = writable();
export const musicVideoURL: Writable<string | undefined> = writable();
export const musicVideoLoaded: Writable<boolean> = writable(false);
export const musicVideoState: Writable<boolean> = writable(false);
export const suggestionsState: Writable<boolean> = writable(false);

// Lyrics state and data
export const lyricsState: Writable<LyricsState | null> = writable(null);
export const lyrics: Writable<Lyrics> = writable();
export const syncedLyrics: Writable<SyncedLyrics> = writable(); 
export const syncedLyricsState: Writable<boolean> = writable(false);

// Data
export const playerData: Writable<Queue> = writable();
export const searchResults: Writable<resultType> = writable();
export const songResults: Writable<object> = writable();
export const videoResults: Writable<VideoResults[] | undefined> = writable();
export const albumResults: Writable<object> = writable();
export const artistResults: Writable<object> = writable();
export const albumInfo: Writable<object> = writable();
export const albumInfoLoading: Writable<boolean> = writable(true);
export const artistInfo: Writable<object> = writable();
export const artistInfoLoading: Writable<boolean> = writable(true);
export const songInfo: Writable<boolean> = writable();
export const songInfoLoading: Writable<object> = writable();
export const videoInfo: Writable<object> = writable();
export const videoInfoLoading: Writable<boolean> = writable();
export const moreSongsLoading: Writable<boolean> = writable(true);
export const moreSongsInfo: Writable<object> = writable();

// Queue data
export const queue: Writable<Queue[]> = writable([]);
export const queueIndex: Writable<number> = writable(0);

// Quality settings
export const quality: Writable<number> = writable(2);

// export const likedSong: Writable<boolean>
export const audioDuration: Writable<number> = writable(0);
export const activeLyricIndex: Writable<number> = writable(0);