import { currentUser } from '../store';

export const Login = async (email: string, password: string) => {
	return await supabase.auth.signInWithPassword({
		email: email,
		password: password
	});
};

export const CheckSession = async (jwt: string) => {
	const userData = await supabase.auth.getUser(jwt);
	console.log('function called');
	console.log(userData);
	currentUser.set(userData.data.user);
};
