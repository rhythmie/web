import { createServerClient } from '@supabase/ssr';
import { redirect } from '@sveltejs/kit';
import type { Handle } from '@sveltejs/kit';
import { sequence } from '@sveltejs/kit/hooks';

import { PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY } from '$env/static/public';

const supabase: Handle = async ({ event, resolve }) => {
	if (!event.locals) {
		event.locals = {};
	}

	event.locals.supabase = createServerClient(PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY, {
		cookies: {
			get: (key) => event.cookies?.get(key),
			set: (key, value, options) => {
				event.cookies?.set(key, value, { ...options, path: '/' });
			},
			remove: (key, options) => {
				event.cookies?.delete(key, { ...options, path: '/' });
			}
		}
	});

	event.locals.safeGetSession = async () => {
		// First get the session to see if it exists
		const {
			data: { session }
		} = await event.locals.supabase.auth.getSession();

		if (!session) {
			return { session: null, user: null };
		}

		// Then get the user to verify the session
		const {
			data: { user },
			error
		} = await event.locals.supabase.auth.getUser();

		if (error) {
			return { session: null, user: null };
		}

		return { session, user };
	};

	return resolve(event, {
		filterSerializedResponseHeaders(name) {
			return name === 'content-range' || name === 'x-supabase-api-version';
		}
	});
};

const authGuard: Handle = async ({ event, resolve }) => {
	let session = null;
	let user = null;

	if (event.locals?.safeGetSession) {
		const result = await event.locals.safeGetSession();
		session = result.session;
		user = result.user;
	}

	event.locals.session = session;
	event.locals.user = user;

	console.log('sessionnnnnnn: ', session);
	console.log('userrrrrrrrrr: ', user);

	if (!event.locals.session && event.url.pathname === '/') {
		return redirect(303, '/login');
	}

	if (
		event.locals.session &&
		!event.locals.user.is_anonymous &&
		(event.url.pathname === '/login' ||
			event.url.pathname === '/signup' ||
			event.url.pathname === '/anonymous')
	) {
		return redirect(303, '/');
	}

	return resolve(event);
};

export const handle: Handle = sequence(supabase);
