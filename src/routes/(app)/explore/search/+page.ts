import { isLoading, searchQuery, searchResults } from '../../../../store';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ url, fetch }) => {
	isLoading.set({ search: true });
	const query = url.searchParams.get('query');
	searchQuery.set(query);
	if (query) {
		try {
			const response = await fetch(`https://rhythmie-api.vercel.app/api/search?query=${query}`, {
				headers: {
					origin: window.location.origin // Get origin from browser window
				}
			});

			if (!response.ok) {
				throw new Error(`API request failed with status ${response.status}`);
			}

			const jsonData = await response.json();
			searchResults.set(jsonData);
			isLoading.set({ search: false });
		} catch (error) {
			console.error('Error fetching data:', error);
		}
	}
};
